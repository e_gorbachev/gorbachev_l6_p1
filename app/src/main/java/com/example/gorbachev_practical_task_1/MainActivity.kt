package com.example.gorbachev_practical_task_1

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.os.PersistableBundle
import android.provider.MediaStore
import android.text.Editable
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.example.gorbachev_practical_task_1.fragments.EditFragment
import com.example.gorbachev_practical_task_1.fragments.MainFragment
import java.util.regex.Matcher
import java.util.regex.Pattern

var bool:Boolean = true
var pic:Bitmap? = null

class MainActivity : AppCompatActivity(), Communicator {
	
	
	var CAMERA_PERMISSION_CODE = 101
	
	private var btnEdit:Button? = null
	private var avatar:ImageView? = null
	private val mainFragment = MainFragment()
	private val editFragment = EditFragment()
	private var btnSave:Button? = null
	
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		
		setSupportActionBar(findViewById(R.id.mainToolbar))
		supportActionBar?.setTitle(0)
		
		btnEdit = findViewById(R.id.changeFrBtn)
		btnSave = findViewById(R.id.saveFrBtn)
		avatar = findViewById(R.id.avatar)
		
		
		onMainFr()
		
		btnEdit?.setOnClickListener {
				onEditFr()
				btnEdit?.visibility = View.GONE
				btnSave?.visibility = View.VISIBLE
		}
		
		avatar?.setOnClickListener{
			if(bool == false) {
				startRequest()
			}
		}
		
	}
	
	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (requestCode == CAMERA_PERMISSION_CODE) {
			pic = data?.getParcelableExtra("data")
			if(pic != null) {
				avatar?.setImageBitmap(pic)
			} else {
				Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show()
			}
		}
	}
	
	private fun onMainFr(){
		supportFragmentManager.beginTransaction().apply {
			replace(R.id.fragmentPlace, mainFragment, "mainFr")
			commit()
			btnEdit?.isVisible = true
			bool = true
		}
	}
	
	private fun onEditFr(){
		supportFragmentManager.beginTransaction().apply {
			replace(R.id.fragmentPlace, editFragment, "editFr")
			commit()
			btnEdit?.isVisible = false
			bool = false
		}
	}
	
	
	// request permission for camera ///////////////////////
	
	private fun startRequest() {
		checkForPermissions(android.Manifest.permission.CAMERA, "camera", CAMERA_PERMISSION_CODE)
	}
	
	private fun checkForPermissions(permission: String, name: String, requestCode: Int) {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			when {
				ContextCompat.checkSelfPermission(applicationContext, permission) == PackageManager.PERMISSION_GRANTED -> {
					changePhotoActivity()
				}
				shouldShowRequestPermissionRationale(permission) -> showDialog(permission,name, requestCode)
				
				else -> ActivityCompat.requestPermissions(this, arrayOf(permission),requestCode)
			}
		}
	}
	
	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		fun innerCheck(name: String) {
			if(grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
				Toast.makeText(applicationContext,"$name permission refused", Toast.LENGTH_SHORT).show()
			} else {
				Toast.makeText(applicationContext,"$name permission granted", Toast.LENGTH_SHORT).show()
				changePhotoActivity()
			}
		}
		innerCheck("camera")
	}
	

	private fun showDialog(permission: String, name:String, requestCode: Int){
		
		val bulder = AlertDialog.Builder(this)
		
		bulder.apply {
			setMessage("Permission for access your $name")
			setTitle("Permission")
			setPositiveButton("Ok"){dialog,which ->
				ActivityCompat.requestPermissions(this@MainActivity, arrayOf(permission),requestCode)
			}
		}
		val dialog = bulder.create()
		dialog.show()
	}
	
	
	
	// photo change //////////////////////////////////////////////
	
	private fun changePhotoActivity() {
		var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
		startActivityForResult(intent,CAMERA_PERMISSION_CODE )
	}
	
	
	
	// fragments communication ////////////////////////////////////
	
	
	override fun passDataCom(name: String, phone:String, email:String){
		val bundle = Bundle()
		bundle.putString("name", name)
		bundle.putString("phone", phone)
		bundle.putString("email", email)
		
		mainFragment.arguments = bundle
		
		onMainFr()
		btnSave?.visibility = View.GONE
	}
	
	
	
	// save state ////////////////////////////////////////////////
	
	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		if (bool == true) {
			val name: String = findViewById<TextView>(R.id.tvName).text.toString()
			val phone: String = findViewById<TextView>(R.id.tvPhone).text.toString()
			val email: String = findViewById<TextView>(R.id.tvEmail).text.toString()
			outState.putString("name", name)
			outState.putString("phone", phone)
			outState.putString("email", email)
			outState.putParcelable("BitmapImage", pic)
		} else {
			val nameInput: String = findViewById<EditText>(R.id.input_name).text.toString()
			val phoneInput: String = findViewById<EditText>(R.id.input_phone).text.toString()
			val emailInput: String = findViewById<EditText>(R.id.input_email).text.toString()
			outState.putString("nameInput", nameInput)
			outState.putString("phoneInput", phoneInput)
			outState.putString("emailInput", emailInput)
			outState.putParcelable("BitmapImage", pic);
		}
	}
	
	override fun onRestoreInstanceState(savedInstanceState: Bundle) {
		super.onRestoreInstanceState(savedInstanceState)
		if (savedInstanceState != null) {
			savedInstanceState.let {
				if (bool) {
					val name = it.get("name") as String?
					val phone = it.get("phone") as String?
					val email = it.get("email") as String?
					val image = it.get("BitmapImage") as Bitmap?
					findViewById<TextView>(R.id.tvName)?.text = name
					findViewById<TextView>(R.id.tvPhone)?.text = phone
					findViewById<TextView>(R.id.tvEmail)?.text = email
					if(image!=null){
						avatar?.setImageBitmap(image)
					}
				} else {
					val nameInput = it.get("nameInput") as Editable?
					val phoneInput = it.get("phoneInput") as Editable?
					val emailInput = it.get("emailInput") as Editable?
					findViewById<EditText>(R.id.input_name)?.text = nameInput
					findViewById<EditText>(R.id.input_phone)?.text = phoneInput
					findViewById<EditText>(R.id.input_email)?.text = emailInput
				}
			}
		}
	}
	
	
}