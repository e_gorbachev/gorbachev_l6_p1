package com.example.gorbachev_practical_task_1.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.gorbachev_practical_task_1.R

class MainFragment : Fragment(R.layout.main_fragment) {
	
	private var name:String? = ""
	private var phone:String? = ""
	private var email:String? = ""
	private var callButton: Button? = null
	private var emailButton:Button? = null

	
	@SuppressLint("CutPasteId")
	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		val view = inflater.inflate(R.layout.main_fragment, container, false)
		
		name = arguments?.getString("name")
		phone = arguments?.getString("phone")
		email = arguments?.getString("email")
		
		view.findViewById<TextView>(R.id.tvName).text = name
		view.findViewById<TextView>(R.id.tvPhone).text = phone
		view.findViewById<TextView>(R.id.tvEmail).text = email
		
		callButton = view.findViewById(R.id.callButton)
		callButton?.setOnClickListener{
			val intent = Intent(Intent.ACTION_DIAL)
			val tel = view.findViewById<TextView>(R.id.tvPhone).text.toString()
			if (tel == ""){
				Toast.makeText(context,"Enter number",Toast.LENGTH_SHORT).show()
			} else {
				Toast.makeText(context, tel,Toast.LENGTH_SHORT).show()
				intent.setData(Uri.parse("tel:$tel"))
				startActivity(intent)
			}
		}
		
		emailButton = view.findViewById(R.id.emailButton)
		emailButton?.setOnClickListener{
			val intent = Intent(Intent.ACTION_SENDTO)
			val email = view.findViewById<TextView>(R.id.tvEmail).text.toString()
			val phone = view.findViewById<TextView>(R.id.tvPhone).text.toString()
			val name = view.findViewById<TextView>(R.id.tvName).text.toString()
			
			if (email==""){
				Toast.makeText(context,"Enter email",Toast.LENGTH_SHORT).show()
			} else {
				Toast.makeText(context, email,Toast.LENGTH_SHORT).show()
				intent.data = Uri.parse("mailto:")
				intent.putExtra(Intent.EXTRA_TEXT, "$name \n phone: $phone \n email: $email")
				intent.putExtra(Intent.EXTRA_SUBJECT, "Hello World")
				startActivity(intent)
			}
		}
		
		
		return view
	}
	
	
	
//	override fun onSaveInstanceState(outState: Bundle) {
//		super.onSaveInstanceState(outState)
//			val name: String = view?.findViewById<TextView>(R.id.tvName)?.text.toString()
//			val phone: String = view?.findViewById<TextView>(R.id.tvPhone)?.text.toString()
//			val email: String = view?.findViewById<TextView>(R.id.tvEmail)?.text.toString()
//			outState.putString("name", name)
//			outState.putString("phone", phone)
//			outState.putString("email", email)
//	}
//
//	override fun onCreate(savedInstanceState: Bundle?) {
//		super.onCreate(savedInstanceState)
//		if (savedInstanceState != null) {
//			val name = savedInstanceState.getString("name", "Name")
//			val phone = savedInstanceState.getString("phone", "phone")
//			val email = savedInstanceState.getString("email", "email")
//			view?.findViewById<TextView>(R.id.tvName)?.text = name
//			view?.findViewById<TextView>(R.id.tvPhone)?.text = phone
//			view?.findViewById<TextView>(R.id.tvEmail)?.text = email
//		}
//	}
	
}