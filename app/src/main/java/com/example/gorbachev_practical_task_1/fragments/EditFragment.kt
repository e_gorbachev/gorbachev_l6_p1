package com.example.gorbachev_practical_task_1.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.gorbachev_practical_task_1.R
import com.example.gorbachev_practical_task_1.Communicator
import com.example.gorbachev_practical_task_1.bool
import java.util.jar.Attributes
import java.util.regex.Matcher
import java.util.regex.Pattern

class EditFragment : Fragment(R.layout.edit_fragment) {
	
	
	
	private lateinit var Communicator: Communicator
	
	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		val view = inflater.inflate(R.layout.edit_fragment, container, false)
		
		
		Communicator = activity as Communicator
		
		var btn = activity?.findViewById<Button>(R.id.saveFrBtn)
		btn?.setOnClickListener {
			val Name = view.findViewById<EditText>(R.id.input_name).text.toString()
			val Phone = view.findViewById<EditText>(R.id.input_phone).text.toString()
			val Email = view.findViewById<EditText>(R.id.input_email).text.toString()
			if (emailValidator(Email)){
				Communicator.passDataCom(Name, Phone, Email)
			} else {
				Toast.makeText(context,"Incorrect information",Toast.LENGTH_SHORT).show()
			}
		}
		return view
	}
	
	
	private fun emailValidator(email: String): Boolean {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
	}
}